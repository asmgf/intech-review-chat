package com.intech.review.chat.service.api;

import com.intech.review.chat.model.impl.Message;

import java.util.List;

public interface MessageService {
    List<Message> getLatest(long id, int limit);
    Message addMessage(String userName, String text);
}

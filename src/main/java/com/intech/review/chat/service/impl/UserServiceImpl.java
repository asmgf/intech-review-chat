package com.intech.review.chat.service.impl;

import com.intech.review.chat.dao.api.UserDao;
import com.intech.review.chat.model.impl.User;
import com.intech.review.chat.service.api.SecurityService;
import com.intech.review.chat.service.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    @Qualifier("passwordEncoder")
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional
    public boolean add(User user) {
        if (userDao.existsWithName(user.getName())) {
            return false;
        }
        encodePassword(user);
        userDao.persist(user);
        return true;
    }

    @Override
    public User findByName(String name) {
        return userDao.findByName(name);
    }

    @Override
    @Transactional
    public boolean updateNameAndPassword(String userName, User user) {
        if (!userName.equals(user.getName()) && userDao.existsWithName(user.getName())) {
            return false;
        }
        User actualUser = userDao.findByName(userName);
        actualUser.setName(user.getName());
        actualUser.setPassword(user.getPassword());
        encodePassword(actualUser);
        userDao.update(actualUser);
        securityService.updateAuthentication(actualUser);
        return true;
    }

    private void encodePassword(User user) {
        String encodedPassword = passwordEncoder.encodePassword(user.getPassword(), null);
        user.setPassword(encodedPassword);
    }
}

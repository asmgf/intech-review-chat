package com.intech.review.chat.service.impl;

import com.intech.review.chat.dao.api.MessageDao;
import com.intech.review.chat.dao.api.UserDao;
import com.intech.review.chat.model.impl.Message;
import com.intech.review.chat.model.impl.User;
import com.intech.review.chat.service.api.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {
    @Autowired
    private UserDao userDao;

    @Autowired
    private MessageDao messageDao;

    public List<Message> getLatest(long id, int limit) {
        if (id < 0) {
            return getLatest(limit);
        }
        if (limit >= 0) {
            return messageDao.getLatestAfterIdWithLimit(id, limit);
        } else {
            return messageDao.getLatestAfterId(id);
        }
    }

    private List<Message> getLatest(int limit) {
        if (limit >= 0) {
            return messageDao.getLatestWithLimit(limit);
        } else {
            return messageDao.getLatest();
        }
    }

    @Override
    @Transactional
    public Message addMessage(String userName, String text) {
        User user = userDao.findByName(userName);
        Message message = new Message(user, text);
        messageDao.persist(message);
        return message;
    }
}

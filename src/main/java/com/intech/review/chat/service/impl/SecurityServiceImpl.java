package com.intech.review.chat.service.impl;

import com.intech.review.chat.dao.api.UserDao;
import com.intech.review.chat.model.impl.User;
import com.intech.review.chat.service.api.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Arrays;
import java.util.List;

public class SecurityServiceImpl implements SecurityService {
    @Autowired
    private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        User user = userDao.findByName(name);
        if (user == null) {
            throw new UsernameNotFoundException(name);
        }
        return convert(user);
    }

    @Override
    public void updateAuthentication(User user) {
        Authentication authentication = createAuthentication(user);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    private static org.springframework.security.core.userdetails.User convert(User user) {
        return new org.springframework.security.core.userdetails.User(
                user.getName(), user.getPassword(), buildAuthority(ROLE.USER));
    }

    private Authentication createAuthentication(User user) {
        return createAuthentication(convert(user));
    }

    private Authentication createAuthentication(org.springframework.security.core.userdetails.User user) {
        return new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), user.getAuthorities());
    }

    private static List<GrantedAuthority> buildAuthority(ROLE role) {
        GrantedAuthority authority = new SimpleGrantedAuthority(role.toString());
        return Arrays.asList(authority);
    }

    private static enum ROLE {
        USER;

        public String toString() {
            return "ROLE_" + super.toString();
        }
    }
}

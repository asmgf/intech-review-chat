package com.intech.review.chat.service.api;

import com.intech.review.chat.model.impl.User;

public interface SecurityService extends org.springframework.security.core.userdetails.UserDetailsService {
    void updateAuthentication(User user);
}

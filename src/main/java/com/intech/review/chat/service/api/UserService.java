package com.intech.review.chat.service.api;

import com.intech.review.chat.model.impl.User;

public interface UserService {
    boolean add(User user);
    User findByName(String name);
    boolean updateNameAndPassword(String userName, User user);
}

package com.intech.review.chat.dao.api;

import com.intech.review.chat.model.api.Model;

public interface Dao<E extends Model> {
    long getCount();
    E findById(Object id);
    void persist(E entity);
    E update(E entity);
}

package com.intech.review.chat.dao.api;

import com.intech.review.chat.model.impl.User;

public interface UserDao extends Dao<User> {
    boolean existsWithName(String name);
    User findByName(String name);
}

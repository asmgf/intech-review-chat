package com.intech.review.chat.dao.impl;

import com.intech.review.chat.dao.api.Dao;
import com.intech.review.chat.model.api.Model;
import com.intech.review.chat.model.impl.User;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.lang.reflect.ParameterizedType;

public class DaoImpl<E extends Model> implements Dao<E> {
    private final Class<E> entityClass;
    private final String entityName;

    @PersistenceContext
    protected EntityManager entityManager;

    @SuppressWarnings("unchecked")
    public DaoImpl() {
        ParameterizedType superClass = (ParameterizedType) getClass().getGenericSuperclass();
        entityClass = (Class<E>) superClass.getActualTypeArguments()[0];
        entityName = entityClass.getName();
    }

    @Override
    public long getCount() {
        String query = "select count(*) from " + entityName;
        return entityManager.createQuery(query, Long.class).getSingleResult();
    }

    @Override
    @Transactional
    public void persist(E entity) {
        entityManager.persist(entity);
    }

    @Override
    @Transactional
    public E update(E entity) {
        return entityManager.merge(entity);
    }

    @Override
    public E findById(Object id) {
        return entityManager.find(entityClass, id);
    }

    protected static <E> E getSingleResultOrNull(TypedQuery<E> query) {
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    protected static boolean exists(TypedQuery<Long> query) {
        Long count = query.getSingleResult();
        return count > 0;
    }
}

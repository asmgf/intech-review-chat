package com.intech.review.chat.dao.impl;

import com.intech.review.chat.dao.api.UserDao;
import com.intech.review.chat.model.impl.User;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;

@Repository
public class UserDaoImpl extends DaoImpl<User> implements UserDao {
    @Override
    public boolean existsWithName(String name) {
        String queryString =
                "select count(user) " +
                "from User user " +
                "where user.name = :name";
        TypedQuery<Long> query =
                entityManager.createQuery(queryString, Long.class)
                .setParameter("name", name);
        return exists(query);
    }

    @Override
    public User findByName(String name) {
        String queryString =
                "select user " +
                "from User user " +
                "where user.name = :name";
        TypedQuery<User> query =
                entityManager.createQuery(queryString, User.class)
                .setParameter("name", name);
        return getSingleResultOrNull(query);
    }
}

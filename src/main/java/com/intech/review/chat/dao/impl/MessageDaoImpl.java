package com.intech.review.chat.dao.impl;

import com.intech.review.chat.dao.api.MessageDao;
import com.intech.review.chat.model.impl.Message;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.Iterator;
import java.util.List;

@Repository
public class MessageDaoImpl extends DaoImpl<Message> implements MessageDao {
    public List<Message> getLatestAfterId(long id) {
        List<Message> messages = getLatestAfterIdQuery(id).getResultList();
        return trimById(messages, id);
    }

    public List<Message> getLatestAfterIdWithLimit(long id, int limit) {
        List<Message> messages = getLatestAfterIdQuery(id).setMaxResults(limit).getResultList();
        return trimById(messages, id);
    }

    @Override
    public List<Message> getLatest() {
        return getLatestQuery().getResultList();
    }

    public List<Message> getLatestWithLimit(int limit) {
        return getLatestQuery().setMaxResults(limit).getResultList();
    }

    private TypedQuery<Message> getLatestQuery() {
        String queryString = "select message from Message message order by timestamp desc";
        return entityManager.createQuery(queryString, Message.class);
    }

    private TypedQuery<Message> getLatestAfterIdQuery(long id) {
        String queryString =
                "select message " +
                "from Message message " +
                "where message.timestamp >= (" +
                    "select message.timestamp " +
                    "from Message message " +
                    "where id = :id" +
                ")" +
                "order by timestamp desc";
        return entityManager.createQuery(queryString, Message.class).setParameter("id", id);
    }

    private static List<Message> trimById(List<Message> messages, long id) {
        boolean found = false;
        for (Iterator<Message> it = messages.iterator(); it.hasNext(); ) {
            Message message = it.next();
            found = found || message.getId() == id;
            if (found) {
                it.remove();
            }
        }
        return messages;
    }
}

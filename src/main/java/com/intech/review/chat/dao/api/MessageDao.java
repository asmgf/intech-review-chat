package com.intech.review.chat.dao.api;

import com.intech.review.chat.model.impl.Message;

import java.util.List;

public interface MessageDao extends Dao<Message> {
    List<Message> getLatest();
    List<Message> getLatestWithLimit(int limit);
    List<Message> getLatestAfterId(long id);
    List<Message> getLatestAfterIdWithLimit(long id, int limit);
}

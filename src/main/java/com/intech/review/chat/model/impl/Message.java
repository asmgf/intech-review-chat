package com.intech.review.chat.model.impl;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Calendar;

@Entity
public class Message extends LongIdModel {
    @Column(length = 512, nullable = false)
    private String text;

    @Column(nullable = false)
    private Timestamp timestamp;

    @ManyToOne
    @JoinColumn(nullable = false)
    private User user;

    public Message() {
    }

    public Message(User user, String text) {
        this.user = user;
        this.text = text;
    }

    @PrePersist
    public void prePersist() {
        Calendar calendar = Calendar.getInstance();
        timestamp = new Timestamp(calendar.getTimeInMillis());
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

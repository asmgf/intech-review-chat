package com.intech.review.chat.model.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "usr")
public class User extends LongIdModel {
    @Column(unique = true, nullable = false, length = 32)
    private String name;

    @Column(nullable = false, length = 40)
    private String password;

    @Column(nullable = false, length = 32)
    private String nick;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }
}

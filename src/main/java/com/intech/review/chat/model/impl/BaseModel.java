package com.intech.review.chat.model.impl;

import com.intech.review.chat.model.api.Model;

public abstract class BaseModel<I> implements Model<I> {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseModel)) return false;
        BaseModel otherModel = (BaseModel) o;
        return objectsEquals(getId(), otherModel.getId());
    }

    @Override
    public int hashCode() {
        I id = getId();
        return id != null ? id.hashCode() : 0;
    }

    public static boolean objectsEquals(Object a, Object b) {
        return (a == b) || (a != null && a.equals(b));
    }
}

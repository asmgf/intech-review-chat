package com.intech.review.chat.model.api;

public interface Model<I> {
    I getId();
}

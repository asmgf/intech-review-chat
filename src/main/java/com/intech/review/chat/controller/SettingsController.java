package com.intech.review.chat.controller;

import com.intech.review.chat.model.impl.User;
import com.intech.review.chat.service.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

@Controller
public class SettingsController extends BaseController {
    public static final String HOME_PATH = "/settings";

    @Autowired
    private UserService userService;

    @RequestMapping(HOME_PATH)
    public String home(Model model, Principal principal) {
        String userName = principal.getName();
        User user = userService.findByName(userName);
        user.setPassword("");
        model.addAttribute("chatPath", url(ChatController.HOME_PATH));
        model.addAttribute("user", user);
        return "settings";
    }

    @RequestMapping(value = HOME_PATH, method = RequestMethod.POST)
    public String change(Model model, @ModelAttribute User user, Principal principal) {
        String userName = principal.getName();
        boolean changed = userService.updateNameAndPassword(userName, user);
        if (changed) {
            return redirect(HOME_PATH);
        }
        model.addAttribute("failed", true);
        return "settings";
    }
}

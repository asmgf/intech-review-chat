package com.intech.review.chat.controller.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LimitSelect {
    private static final List<LimitOption> options;

    static {
        LimitOption[] itemsArray = new LimitOption[]{
                new LimitOption(2),
                new LimitOption(5),
                new LimitOption(50),
                new LimitOption(100),
                new LimitOption(200),
                new LimitOption(-1, "Все")
        };
        options = Collections.unmodifiableList(Arrays.asList(itemsArray));
    }

    public List<LimitOption> getOptions() {
        return options;
    }

    public static class LimitOption {
        private final int value;
        private final String name;

        public LimitOption(int value) {
            this.value = value;
            this.name = String.valueOf(value);
        }

        public LimitOption(int value, String name) {
            this.value = value;
            this.name = name;
        }

        public int getValue() {
            return value;
        }

        public String getName() {
            return name;
        }
    }
}

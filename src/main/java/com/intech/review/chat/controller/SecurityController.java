package com.intech.review.chat.controller;

import com.intech.review.chat.model.impl.User;
import com.intech.review.chat.service.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SecurityController extends BaseController {
    public static final String LOG_IN_PATH = "/login";
    public static final String LOG_OUT_PATH = "/logout";
    public static final String SING_UP_PATH = "/sing-up";

    @Autowired
    private UserService userService;

    @RequestMapping(value = LOG_IN_PATH, method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("springSecurityLogInPath", url("/j_spring_security_check"));
        model.addAttribute("singUpPath", url(SING_UP_PATH));
        return "login";
    }

    @RequestMapping(value = SING_UP_PATH, method = RequestMethod.GET)
    public String singUp(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("logInPath", url(LOG_IN_PATH));
        model.addAttribute("singUpFormAction", url(SING_UP_PATH));
        return "sing-up";
    }

    @RequestMapping(value = SING_UP_PATH, method = RequestMethod.POST)
    public String handleSingUp(Model model, @ModelAttribute User user) {
        boolean added = userService.add(user);
        if (added) {
            return redirect(LOG_IN_PATH);
        }
        model.addAttribute("failure", true);
        return "sing-up";
    }
}

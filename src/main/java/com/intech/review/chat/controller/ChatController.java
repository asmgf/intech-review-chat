package com.intech.review.chat.controller;

import com.intech.review.chat.controller.model.LimitSelect;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ChatController extends BaseController {
    public static final String HOME_PATH = "/";
    public static final String WITH_LIMIT_PATH = "/{limit}";

    @RequestMapping(HOME_PATH)
    public String chat(Model model) {
        return chatWithLimit(model, -1);
    }

    @RequestMapping(WITH_LIMIT_PATH)
    public String chatWithLimit(Model model, @PathVariable int limit) {
        model.addAttribute("logOutPath", url(SecurityController.LOG_OUT_PATH));
        model.addAttribute("settingsPath", url(SettingsController.HOME_PATH));
        model.addAttribute("getMessagesPath", url(MessageController.GET_PATH));
        model.addAttribute("addMessagePath", url(MessageController.ADD_PATH));
        model.addAttribute("chatPath", url(HOME_PATH));
        model.addAttribute("chatWithLimitPath", url(WITH_LIMIT_PATH));
        model.addAttribute("messagesLimit", limit);
        model.addAttribute("limitSelect", new LimitSelect());
        return "chat";
    }
}

package com.intech.review.chat.controller;

import com.intech.review.chat.controller.model.ChatMessage;
import com.intech.review.chat.model.impl.Message;
import com.intech.review.chat.service.api.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MessageController {
    public static final String GET_PATH = "/messages/from/{id}/limit/{limit}";
    public static final String ADD_PATH = "/messages/add";

    @Autowired
    private MessageService messageService;

    @RequestMapping(GET_PATH)
    public @ResponseBody List<ChatMessage> get(@PathVariable long id, @PathVariable int limit) {
        return convert(messageService.getLatest(id, limit));
    }

    @RequestMapping(value = ADD_PATH, method = RequestMethod.POST)
    public @ResponseBody ChatMessage add(@RequestParam String text, Principal principal) {
        Message message = messageService.addMessage(principal.getName(), text);
        return convert(message);
    }

    private static List<ChatMessage> convert(List<Message> messages) {
        List<ChatMessage> chatMessages = new ArrayList<ChatMessage>(messages.size());
        for (Message message : messages) {
            chatMessages.add(convert(message));
        }
        return chatMessages;
    }

    private static ChatMessage convert(Message message) {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setId(message.getId());
        chatMessage.setTimestamp(message.getTimestamp().getTime());
        chatMessage.setNick(message.getUser().getNick());
        chatMessage.setText(message.getText());
        return chatMessage;
    }
}

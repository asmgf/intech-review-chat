package com.intech.review.chat.controller;

import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

class BaseController {
    @Autowired
    private HttpServletRequest httpServletRequest;

    protected String url(String url) {
        return httpServletRequest.getContextPath() + url;
    }

    protected String redirect(String url) {
        return "redirect:" + url;
    }
}

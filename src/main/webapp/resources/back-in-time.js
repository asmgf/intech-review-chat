(function () {

var SECOND = 1000,
    MINUTE = 60 * SECOND,
    HOUR = 60 * MINUTE,
    DAY = 24 * HOUR,
    MONTH = 30 * DAY,
    YEAR = 12 * MONTH,
    SCALES = [YEAR, MONTH, DAY, HOUR, MINUTE],
    MIN_SCALE = SCALES[SCALES.length - 1],
    INTERVAL = 1000;

function toScales(timestamp) {
    var result = {};
    $.each(SCALES, function (i, scale) {
        result[scale] = Math.floor(timestamp / scale);
        result[scale] = Math.max(0, result[scale]);
        timestamp = timestamp % scale;
    });
    result[MIN_SCALE] = Math.max(1, result[MIN_SCALE]);
    return result;
}

function toString(onScales) {
    return '' +
        onScales[YEAR] + 'г ' +
        onScales[MONTH] + 'м ' +
        onScales[DAY] + 'д ' +
        onScales[HOUR] + 'ч ' +
        onScales[MINUTE] + 'м';
}

function handle($element) {
    var timestamp = $element.data('back-in-time-timestamp'),
        delta = new Date().getTime() - timestamp,
        onScales = toScales(delta),
        text = toString(onScales);
    return $element.text(text);
}

window.setInterval(function () {
    $('.back-in-time').each(function (i, element) {
        handle($(element));
    });
}, INTERVAL);

window.backInTime = function ($element, timestamp) {
    $element.data('back-in-time-timestamp', timestamp);
    return handle($element).addClass('back-in-time');
};

})();

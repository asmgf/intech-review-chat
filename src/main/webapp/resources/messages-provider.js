(function () {

function nop() {}
function logError() { console.error('error'); }

function MessagesProvider(settings) {
    var baseCallback = settings.callback;
    var url = settings.url;
    var latestId = -1;
    var intervalId = -1;
    var limit = -1;
    var inProgress = false;

    function buildUrl() {
        return url.replace('{id}', latestId).replace('{limit}', limit);
    }

    function internalCallback(messages) {
        if (messages.length === 0) {
            return;
        }
        var latestMessage = messages[0];
        latestId = latestMessage.id;
    }

    function requestNew() {
        if (inProgress) {
            return;
        }

        inProgress = true;
        $.ajax({
            url: buildUrl(),
            dataType: 'json'
        })
        .done(internalCallback)
        .done(baseCallback)
        .error(logError)
        .always(function () {
            inProgress = false;
        });
    }

    function setInterval(interval) {
        if (intervalId !== -1) {
            window.clearInterval(intervalId);
        }
        intervalId = window.setInterval(function () {
            console.log('MessagesProvider.interval');
            requestNew();
        }, interval);
    }

    function setLimit(newLimit) {
        limit = newLimit;
    }

    return {
        setLimit: setLimit,
        requestNew: requestNew,
        setInterval: setInterval
    };
}


window.MessagesProvider = MessagesProvider;

})();

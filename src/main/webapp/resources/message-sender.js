(function () {

function logError() { console.error('error'); }

function MessageSender(settings) {
    var url = settings.url;

    function send(message, callback) {
        $.ajax({
            type: 'POST',
            url: url,
            data: message,
            dataType: 'json'
        })
        .done(callback)
        .error(logError);
    }

    return {
        send: send
    };
}

window.MessageSender = MessageSender;

})();

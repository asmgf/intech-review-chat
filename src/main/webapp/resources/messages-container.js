(function () {

function MessagesContainer($element) {
    var count = 0;
    var limit = -1;

    function addAll(messages) {
        for (var i = messages.length - 1; i >= 0; --i) {
            add(messages[i]);
        }
    }

    function add(message) {
        var $view = createView(message);
        ensureCapacity();
        $element.append($view);
        count += 1;
    }

    function createTimestampView(timestamp) {
        return backInTime($('<span/>'), timestamp);
    }

    function createTextView(text) {
        return $('<span/>').text(text);
    }

    function createView(message) {
        var $timestampView = createTimestampView(message.timestamp),
            text = ' (' + message.nick + '): ' + message.text,
            $textView = createTextView(text);
        return $('<div/>').append($timestampView).append($textView);
    }

    function setLimit(newLimit) {
        limit = newLimit;
    }

    function ensureCapacity() {
        if (limit === -1) {
            return;
        }
        if (count + 1 <= limit) {
            return;
        }
        $element.find(':first').remove();
        count -= 1;
        console.log('MessagesContainer.ensureCapacity: count=' + count);
    }

    return {
        addAll: addAll,
        setLimit: setLimit
    };
}

window.MessagesContainer = MessagesContainer;

})();

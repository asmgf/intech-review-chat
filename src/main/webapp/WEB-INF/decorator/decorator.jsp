<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chat</title>
</head>
<body>
    <sitemesh:write property='body'/>

    <c:set var="root" value="${pageContext.request.contextPath}"/>
    <c:if test="${pageContext.request.contextPath == '/'}">
        <c:set var="root" value=""/>
    </c:if>
    <script src="${root}/static/jquery-1.11.1.min.js"></script>
    <script src="${root}/static/back-in-time.js"></script>
    <script src="${root}/static/messages-provider.js"></script>
    <script src="${root}/static/messages-container.js"></script>
    <script src="${root}/static/message-sender.js"></script>
    <sitemesh:write property='head'/>
</body>
</html>

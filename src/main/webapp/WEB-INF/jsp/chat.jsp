<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<html>
    <head>
        <script>
            var $messagesBlock = $('.messages'),
                messagesContainer = new MessagesContainer($messagesBlock),
                messagesProvider = new MessagesProvider({
                    url: '${getMessagesPath}',
                    callback: function (messages) {
                        messagesContainer.addAll(messages);
                    }
                }),
                messageSender = new MessageSender({
                    url: '${addMessagePath}'
                });
                $sendMessageForm = $('.send-message'),
                $sendMessageTextInput = $sendMessageForm.find('input:text'),
                $limitSelect = $('select.messages-limit');

            messagesContainer.setLimit(${messagesLimit});
            messagesProvider.setLimit(${messagesLimit});
            messagesProvider.requestNew();
            messagesProvider.setInterval(2000);

            $sendMessageForm.submit(function (event) {
                var text = $sendMessageTextInput.val();
                messageSender.send(
                    { text: text },
                    function () {
                        $sendMessageTextInput.val('');
                        messagesProvider.requestNew();
                    }
                );
                event.preventDefault();
            });

            $limitSelect.change(function () {
                var href;
                var limit = $limitSelect.find('option:selected').val();
                if (limit === '-1') {
                    href = '${chatPath}';
                } else {
                    href = '${chatWithLimitPath}'.replace('{limit}', limit);
                }
                window.location.href = href;
            });
        </script>
    </head>
    <body>
        <a href="${settingsPath}">Профиль</a>
        <a href="${logOutPath}">Выйти</a>

        <div class="messages"></div>
        <form class="send-message">
            <input type="text" />
            <input type="submit" value="Отправить" />
            <select class="messages-limit">
                <c:forEach var="option" items="${limitSelect.options}">
                    <option value="${option.value}"
                        <c:if test="${option.value == messagesLimit}">selected="selected"</c:if>
                    >
                        ${option.name}
                    </option>
                </c:forEach>
            </select>
        </form>
    </body>
</html>

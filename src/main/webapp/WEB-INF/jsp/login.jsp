<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<html>
    <body>
        <div>
            <a href="${singUpPath}">Регистрация</a>
        </div>

        <c:if test="${not empty param.failure}">
            <div>Проверьте логин и пароль и попробуйте еще раз</div>
        </c:if>

        <c:if test="${not empty param.logout}">
            <div>Вы вышли из чата</div>
        </c:if>

        <form action="${springSecurityLogInPath}" method="POST">
            <label>Логин</label>
            <input type="text" name="j_username" />
            <label>Пароль</label>
            <input type="password" name="j_password" />
            <input type="submit" value="Войти" />
        </form>
    </body>
</html>

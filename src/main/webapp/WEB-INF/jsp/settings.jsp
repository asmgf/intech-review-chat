<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<html>
    <body>
        <div>
            <a href="${chatPath}">Чат</a>
        </div>

        <f:form commandName="user">
            <label>Логин:&nbsp;</label>
            <f:input path="name" />

            <label>Пароль:&nbsp;</label>
            <f:input type="password" path="password" />

            <c:if test="${not empty failed}">
                <div>Имя уже занято</div>
            </c:if>

            <input type="submit" value="Сохранить" />
        </f:form>
    </body>
</html>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<html>
    <body>
        <div>
            <a href="${logInPath}">Вход</a>
        </div>

        <c:if test="${not empty failure}">
            <div>Такой логин уже занят</div>
        </c:if>

        <f:form commandName="user" action="${singUpFormAction}" method="POST">
            <label>Ник</label>
            <f:input path="nick" />
            <label>Логин</label>
            <f:input path="name" />
            <label>Пароль</label>
            <f:input type="password" path="password" />
            <input type="submit" value="Зарегистрироваться" />
        </f:form>
    </body>
</html>
